#include "functions.h"

int indexMinElement( QVector<int> &vec)
{
    int min = vec[0]; int indexmin = 0;
    for (int i = 1; i < vec.size(); i++)
    {
       if (vec[i] < min)
       {
           min = vec[i];
           indexmin = i;
       }
    }
    return indexmin;
}
