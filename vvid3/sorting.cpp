#include "functions.h"

QVector<int> sorting( QVector<int> &vector1)
{
    for(int i = 0; i < vector1.size(); i++)
    {
        for (int j = 0; j < vector1.size(); j++)
        {
                if(vector1.at(i) < vector1.at(j))
                {
                    int tmp = vector1.at(i);
                    vector1[i] = vector1.at(j);
                    vector1[j] = tmp;
                }
        }
    }
    return  vector1;
}
