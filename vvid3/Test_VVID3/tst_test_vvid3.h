#ifndef TST_TEST_VVID3_H
#define TST_TEST_VVID3_H

#include <QtTest>

class tst_test_vvid3 : public QObject
{
    Q_OBJECT

private slots:
    void test_case1();
    void test_case2();
    void test_case3();
    void test_case4();
    void test_case5();
    void test_case6();
};

#endif // TST_TEST_VVID3_H
