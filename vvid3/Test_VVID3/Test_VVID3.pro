QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_tst_test_vvid3.cpp

SOURCES += $$files(../*.cpp)

SOURCES -= ../main.cpp

HEADERS += \
    tst_test_vvid3.h

HEADERS += ../*.h
