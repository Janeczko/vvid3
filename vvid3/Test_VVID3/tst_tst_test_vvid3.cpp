#include <../functions.h>
#include "tst_test_vvid3.h"

// add necessary includes here

void tst_test_vvid3::test_case1()
{
    QVector<int> test_vec = {1, 2, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    QCOMPARE(0, indexMinElement(test_vec));
}

void tst_test_vvid3::test_case2()
{
    QVector<int> test_vec = {1, 2, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    int test_num = 2;
    QCOMPARE(1, indexNeedElement(test_vec, test_num));
}

void tst_test_vvid3::test_case3()
{
    QVector<int> test_vec = {3, 2, 1};
    QVector<int> test_vec2 = {1, 2, 3};
    QCOMPARE(test_vec2, sorting(test_vec));
}

void tst_test_vvid3::test_case4()
{
    QVector<int> test_vec = {3, 2, 1, 4, 4, 4, 0};
    QVector<int> test_vec2 = {4, 3};
    QCOMPARE(test_vec2, posledovatelnost(test_vec));
}

void tst_test_vvid3::test_case5()
{
    QVector<int> test_vec = {3, 25, 10, 44, 4, 2, 0};
    QVector<int> test_vec2 = {44, 25, 10};
    QCOMPARE(test_vec2, threeMaxElements(test_vec));
}

void tst_test_vvid3::test_case6()
{
    QVector<int> test_vec = {3, 25, 10, 44, 4, 2, 0};
    QVector<int> test_vec2 = {44, 25, 10};
    QCOMPARE(1, sravnenie(test_vec, test_vec2));
}

QTEST_APPLESS_MAIN(tst_test_vvid3)

//#include "tst_tst_test_vvid3.moc"
