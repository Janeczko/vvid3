#include "functions.h"

int indexNeedElement( QVector<int> &vec, int needElement)
{
    for (int i = 0; i < vec.size(); i++)
    {
       if (vec[i] == needElement)
           return i;
    }
    return -1;
}
