#include "functions.h"
#include <limits>

QVector<int> threeMaxElements( QVector<int> &vec)
{
    QVector<int> result; int max1 = INT_MIN+2; int max2 = INT_MIN+1; int max3 = INT_MIN;
    for (int i = 0; i < vec.size(); i++)
    {
       if (vec[i] > max1)
       {
              max3 = max2;
              max2 = max1;
              max1 = vec[i];
       }
      else   if (vec[i] > max2)
       {
          max3 = max2;
          max2 = vec[i];
       }
    }
    if (max3 == INT_MIN)
        max3 = max2;
    result.append(max1); result.append(max2); result.append(max3);
    return (result);
}
