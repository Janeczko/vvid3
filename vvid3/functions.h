#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>

int indexMinElement( QVector<int> &vec);
int indexNeedElement( QVector<int> &vec, int needElement);
QVector<int> threeMaxElements(QVector<int> &vec);
int sravnenie(QVector<int> &vec, QVector<int> &vec2);
QVector<int> sorting( QVector<int> &vec);
QVector<int> posledovatelnost( QVector<int> &vec);

#endif // FUNCTIONS_H
